'use strict'

const { drainLogSync, sortLogByDate } = require('./common/log-handler')

module.exports = (logSources, printer) => {
  const _logSourceChunks = [];

  process.stdin.on('entry', (entryChunk) => {
    _logSourceChunks.push(entryChunk)
  });

  process.on('exit', () => {
    global.gc()
    sortLogByDate(_logSourceChunks, printer)
    printer.done()
  });

  logSources.forEach((logSource) => {
    drainLogSync(logSource)
  });

  process.exit();
};
