'use strict';

const P = require('bluebird'),
  { drainLogAsync, sortLogByDate } = require('./common/log-handler')

module.exports = (logSources, printer) => {
  const _logSourceChunks = []

  process.stdin.on('entry', (entryChunk) => {
    _logSourceChunks.push(entryChunk)
  })

  process.on('exit', () => {
    global.gc()
    sortLogByDate(_logSourceChunks, printer)
    printer.done()
  })

  P.all(logSources)
    .then((logSources) => {
      logSources.forEach((logSource) => {
        drainLogAsync(logSource)
          .then(() => {
            process.exit()
          })
      })
    })
}
