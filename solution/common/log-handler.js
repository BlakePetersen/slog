'use strict'

const arraySort = require('array-sort'),
  P = require('bluebird')

const drainLogAsync = (logSource) => {
  const _resolver = P.defer()
  const _drainLog = () => {
    if (!logSource.drained){
      P.cast(logSource.popAsync())
        .then((logEntry) => {
          logEntry ? process.stdin.emit('entry', logEntry) : false
          return _drainLog()
        })
        .catch(_resolver.reject)
    } else {
      _resolver.resolve()
    }
  }
  process.nextTick(_drainLog)
  return _resolver.promise
}

const drainLogSync = (logSource) => {
  if (!logSource.drained) {
    const _logEntry = logSource.pop()
    _logEntry ? process.stdin.emit('entry', _logEntry) : false
    return drainLogSync(logSource)
  }
}

const sortLogByDate = (logs, printer) => {
  const _sortedLogs = arraySort(logs, 'date')
  _sortedLogs.forEach((log) => {
    // Print each log using printer
    printer.print(log)
  })
}

// I love the revealing module pattern and tend to use it day to day so tossing it in here
// Clean, non-polluting, tree-shake-friendly, flexible, scalable, this-less, awesome =]
module.exports = {
  drainLogAsync: drainLogAsync,
  drainLogSync: drainLogSync,
  sortLogByDate: sortLogByDate
}
