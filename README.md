!["Slog"](https://bytebucket.org/BlakePetersen/slog/raw/d919d213efe51660b452b617d13b1f6def446b2d/slog.jpg?token=4dea60839bae3f5246550791c5cbba338d1707a4 "Slogg'n like a boss")

# Slog
## Solution by Blake Petersen
#### To Run...
* Synchronous: `npm run sync`
* Asynchronous: `npm run async`
* Both at once: `npm run start`
* Test, always test: `npm run test`
